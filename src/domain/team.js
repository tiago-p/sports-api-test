import Joi from "joi";
import joiObjectId from "joi-objectid";
import { ValidationError } from "./errors/validationError";

Joi.objectId = joiObjectId(Joi);

const schema = Joi.object().keys({
  name: Joi.string().required(),
  thumbnail: Joi.string(),
  players: Joi.array().items(Joi.objectId()),
});

export class Team {
  constructor({ _id, name, thumbnail, players }) {
    this._id = _id;
    this.name = name;
    this.thumbnail = thumbnail;
    this.players = players || [];
  }

  static create(data) {
    const result = schema.validate(data, { abortEarly: false });
    if (result.error) {
      throw new ValidationError("Invalid team data", { errors: result.error.details });
    }
    return new Team({
      ...result.value,
    });
  }

  get() {
    return {
      _id: this._id,
      name: this.name,
      thumbnail: this.thumbnail,
      players: this.players,
    };
  }
}
