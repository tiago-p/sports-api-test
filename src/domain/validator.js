export const isValidId = (value) => {
  return typeof value === "string" && value.length === 24 && /^[A-F0-9]+$/i.test(value);
};
