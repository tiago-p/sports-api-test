export class NotFoundError extends Error {
  constructor(message = "Resource not found", data = null) {
    super();
    this.message = message;
    this.data = data;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
