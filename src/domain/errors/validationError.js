export class ValidationError extends Error {
  constructor(message = "Validation error", data = null) {
    super();
    this.message = message;
    this.data = data;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
