export class InvalidArgument extends Error {
  constructor(message = "Invalid request", data = null) {
    super();
    this.message = message;
    this.data = data;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
