export class BadRequestError extends Error {
  constructor(message = "Bad request", data = null) {
    super();
    this.message = message;
    this.data = data;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
