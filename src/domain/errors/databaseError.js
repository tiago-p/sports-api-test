export class DatabaseError extends Error {
  constructor(message = "Database error", data = null) {
    super();
    this.message = message;
    this.data = data;
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
