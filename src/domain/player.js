import Joi from "joi";
import { ValidationError } from "./errors/validationError";

const schema = Joi.object().keys({
  name: Joi.string().required(),
  position: Joi.string().required(),
  thumbnail: Joi.string(),
  born: Joi.date().iso().required(),
  signin: Joi.object({
    amount: Joi.number(),
    currency: Joi.string().valid("eur", "dollar", "gpp"),
  }),
});

export class Player {
  constructor({ _id, name, position, thumbnail, signin, born }) {
    this._id = _id;
    this.name = name;
    this.position = position;
    this.thumbnail = thumbnail;
    this.signin = signin;
    this.born = born;
  }

  static create(data) {
    const result = schema.validate(data, { abortEarly: false });
    if (result.error) {
      throw new ValidationError("Invalid player data", { errors: result.error.details });
    }
    return new Player({
      ...result.value,
    });
  }

  get() {
    return {
      _id: this._id,
      name: this.name,
      position: this.position,
      thumbnail: this.thumbnail,
      signin: this.signin,
      born: this.born,
    };
  }
}
