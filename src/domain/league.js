import Joi from "joi";
import joiObjectId from "joi-objectid";
import { ValidationError } from "./errors/validationError";

Joi.objectId = joiObjectId(Joi);

const schema = Joi.object().keys({
  name: Joi.string().required(),
  sport: Joi.string().required(),
  teams: Joi.array().items(Joi.objectId()),
});

export class League {
  constructor({ _id, name, sport, teams }) {
    this._id = _id;
    this.name = name;
    this.sport = sport;
    this.teams = teams || [];
  }

  static create(data) {
    const result = schema.validate(data, { abortEarly: false });
    if (result.error) {
      throw new ValidationError("Invalid league data", { errors: result.error.details });
    }
    return new League({
      ...result.value,
    });
  }

  get() {
    return {
      _id: this._id,
      name: this.name,
      sport: this.sport,
      teams: this.teams,
    };
  }
}
