export class GetPlayers {
  constructor({ playerRepository }) {
    this.playerRepository = playerRepository;
  }

  async execute() {
    return this.playerRepository.findAll();
  }
}
