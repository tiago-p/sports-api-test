import { Player } from "../domain/player";

export class CreatePlayer {
  constructor({ playerRepository }) {
    this.playerRepository = playerRepository;
  }

  async execute(data) {
    const player = Player.create(data);
    return await this.playerRepository.insert(player);
  }
}
