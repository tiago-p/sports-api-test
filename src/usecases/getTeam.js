import { NotFoundError } from "../domain/errors/notFoundError";
import { ValidationError } from "../domain/errors/validationError";
import { isValidId } from "../domain/validator";

export class GetTeam {
  constructor({ teamRepository }) {
    this.teamRepository = teamRepository;
  }

  async execute(teamId) {
    if (!isValidId(teamId)) {
      throw new ValidationError("Team must be a valid id");
    }
    const team = await this.teamRepository.getTeamWithPlayers(teamId);
    if (!team) {
      throw new NotFoundError("Team not found");
    }
    return team;
  }
}
