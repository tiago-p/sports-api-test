import { NotFoundError } from "../domain/errors/notFoundError";
import { ValidationError } from "../domain/errors/validationError";
import { isValidId } from "../domain/validator";

export class GetLeagueTeams {
  constructor({ teamRepository, leagueRepository }) {
    this.teamRepository = teamRepository;
    this.leagueRepository = leagueRepository;
  }

  async execute(idLeague) {
    if (!isValidId(idLeague)) {
      throw new ValidationError("League must be a valid id");
    }
    const league = await this.leagueRepository.findOne(idLeague);
    if (!league) {
      throw new NotFoundError("League not found");
    }
    return await this.teamRepository.findByLeague(idLeague);
  }
}
