import { ValidationError } from "../domain/errors/validationError";

export class GetLeagues {
  constructor({ leagueRepository }) {
    this.leagueRepository = leagueRepository;
  }

  async execute(query) {
    if (query && typeof query !== "string") {
      throw new ValidationError("Query must be a string value");
    }
    if (query?.trim().length < 2) {
      throw new ValidationError("Search query must contains almost 2 characters");
    }
    return this.leagueRepository.findAll(query?.trim());
  }
}
