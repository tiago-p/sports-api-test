const httpCodeMap = new Map([
  ["BadRequestError", 400],
  ["NotFoundError", 404],
  ["DatabaseError", 500],
  ["ApiRequestError", 500],
  ["ValidationError", 400],
]);

export function errorHandler({ logger }) {
  const getErrorCode = (err) => {
    const type = err.constructor.name;
    const parent = Object.getPrototypeOf(err.constructor).name;
    return httpCodeMap.get(type) || httpCodeMap.get(parent) || 500;
  };

  /* eslint-disable-next-line */
  this.errorHandlerMiddleware = function (error, req, res, next) {
    const code = getErrorCode(error) || 500;

    if (code === 500) {
      logger.error(error.message);
    }

    res.status(code).json({
      message: error.message,
      name: error.name,
      data: error.data || undefined,
    });
  };
}
