import { Router } from "express";

export const routes =
  ({ apiController, teamsController, leaguesController, playersController }) =>
  () => {
    const router = Router();

    router.get("/status", apiController.status.bind(apiController));
    router.get("/leagues", leaguesController.getList.bind(leaguesController));
    router.get("/leagues/:idLeague/teams", teamsController.getList.bind(teamsController));
    router.get("/teams/:idTeam", teamsController.getOne.bind(teamsController));
    router.get("/players", playersController.getList.bind(playersController));
    router.post("/players", playersController.create.bind(playersController));

    return router;
  };
