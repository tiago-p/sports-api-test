import { version, name } from "../../../package.json";
export class ApiController {
  status(req, res) {
    res.json({ name, version });
  }
}
