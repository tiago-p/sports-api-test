export class TeamsController {
  constructor({ getLeagueTeams, getTeam }) {
    this.getLeagueTeams = getLeagueTeams;
    this.getTeam = getTeam;
  }

  async getList(req, res) {
    const { idLeague } = req.params;
    const teams = await this.getLeagueTeams.execute(idLeague);
    res.status(200).json(teams);
  }

  async getOne(req, res) {
    const { idTeam } = req.params;
    const team = await this.getTeam.execute(idTeam);
    res.status(200).json(team.get());
  }
}
