export class LeaguesController {
  constructor({ getLeagues }) {
    this.getLeagues = getLeagues;
  }

  async getList(req, res) {
    const { query } = req.query;
    const leagues = await this.getLeagues.execute(query);
    res.status(200).json(leagues);
  }
}
