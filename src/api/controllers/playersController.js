export class PlayersController {
  constructor({ createPlayer, getPlayers }) {
    this.createPlayer = createPlayer;
    this.getPlayers = getPlayers;
  }

  async create(req, res) {
    const data = req.body;
    const player = await this.createPlayer.execute(data);
    res.status(200).json(player.get());
  }

  async getList(req, res) {
    const players = await this.getPlayers.execute();
    res.status(200).json(players);
  }
}
