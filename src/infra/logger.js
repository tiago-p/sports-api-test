import winston from "winston";
const { combine, timestamp, printf } = winston.format;

const logFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp} ${level}: ${message}`;
});

export const logger = ({ config }) => {
  const logger = winston.createLogger({
    level: config.logLevel,
    format: combine(timestamp(), logFormat),
    transports: [
      new winston.transports.Console({
        name: "console",
        format: winston.format.combine(winston.format.colorize()),
      }),
    ],
  });
  return logger;
};
