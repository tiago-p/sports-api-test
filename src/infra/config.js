import config from "../../config/config.json";
import * as dotenv from "dotenv";
dotenv.config();

config.env = process.env.NODE_ENV || "development";
config.MONGODB_URL = process.env.MONGODB_URL;

export default config;
