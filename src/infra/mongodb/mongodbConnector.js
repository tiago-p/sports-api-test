import { MongoClient } from "mongodb";

export class MongodbConnector {
  async connect(url) {
    const client = new MongoClient(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    await client.connect();

    this.client = client;
    this.db = client.db();
  }

  async disconnect() {
    if (this.client) {
      await this.client.close();
    }
  }
}
