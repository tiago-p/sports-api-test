import { ObjectId } from "mongodb";
import { DatabaseError } from "../../../domain/errors/databaseError";
import { InvalidArgument } from "../../../domain/errors/invalidArgument";
import { Team } from "../../../domain/team";

export class TeamRepository {
  constructor({ mongodbConnector, logger }) {
    this.mongodbConnector = mongodbConnector;
    this.logger = logger;
  }

  async insert(teamModel) {
    if (!(teamModel instanceof Team)) {
      throw new InvalidArgument("Wrong object send to repository");
    }
    const teamPlainObject = teamModel.get();
    try {
      const result = await this.mongodbConnector.db.collection("teams").insertOne({
        ...teamPlainObject,
        players: teamPlainObject.players?.map((id) => ObjectId(id)) || [],
      });

      return new Team({
        ...teamModel.get(),
        _id: result.insertedId.toString(),
      });
    } catch (err) {
      this.logger.error(`TeamRepository.insert error : ${err.message}`);
      throw new DatabaseError(`Database query error`);
    }
  }

  async getTeamWithPlayers(idTeam) {
    if (!ObjectId.isValid(idTeam)) {
      throw new InvalidArgument("Invalid idTeam");
    }
    try {
      const [result] = await this.mongodbConnector.db
        .collection("teams")
        .aggregate([
          {
            $match: {
              _id: ObjectId(idTeam),
            },
          },
          {
            $lookup: {
              from: "players",
              localField: "players",
              foreignField: "_id",
              as: "players",
            },
          },
        ])
        .toArray();

      return result ? new Team({ ...result, _id: result._id.toString() }) : null;
    } catch (err) {
      this.logger.error(`TeamRepository.getTeamWithPlayers error : ${err.message}`);
      throw new DatabaseError("Database query error");
    }
  }

  async findByLeague(idLeague) {
    if (!ObjectId.isValid(idLeague)) {
      throw new InvalidArgument("Invalid idLeague");
    }
    try {
      const result = await this.mongodbConnector.db
        .collection("teams")
        .aggregate([
          {
            $lookup: {
              from: "leagues",
              localField: "_id",
              foreignField: "teams",
              as: "league",
            },
          },
          {
            $match: {
              "league._id": ObjectId(idLeague),
            },
          },
          {
            $project: {
              league: 0,
            },
          },
        ])
        .toArray();

      return result?.map((row) => ({
        ...row,
        _id: row._id.toString(),
        players: row.players?.map((id) => id.toString()) || [],
      }));
    } catch (err) {
      this.logger.error(`TeamRepository.findByLeague error : ${err.message}`);
      throw new DatabaseError("Database query error");
    }
  }
}
