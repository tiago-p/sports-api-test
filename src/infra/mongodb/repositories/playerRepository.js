import { DatabaseError } from "../../../domain/errors/databaseError";
import { InvalidArgument } from "../../../domain/errors/invalidArgument";
import { Player } from "../../../domain/player";

export class PlayerRepository {
  constructor({ mongodbConnector, logger }) {
    this.logger = logger;
    this.mongodbConnector = mongodbConnector;
  }

  async insert(playerModel) {
    if (!(playerModel instanceof Player)) {
      throw new InvalidArgument("Wrong object send to repository");
    }
    try {
      const result = await this.mongodbConnector.db
        .collection("players")
        .insertOne({ ...playerModel.get() });

      return new Player({ ...playerModel.get(), _id: result.insertedId.toString() });
    } catch (err) {
      this.logger.error(`PlayerRepository.insert error : ${err.message}`);
      throw new DatabaseError(`Database query error`);
    }
  }

  async findAll() {
    try {
      const result = await this.mongodbConnector.db
        .collection("players")
        .find()
        .sort({ name: 1 })
        .toArray();

      return result?.map((row) => ({
        ...row,
        _id: row._id.toString(),
      }));
    } catch (err) {
      this.logger.error(`PlayerRepository.findAll error : ${err.message}`);
      throw new DatabaseError("Database query error");
    }
  }
}
