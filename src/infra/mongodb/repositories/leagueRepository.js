import { ObjectId } from "mongodb";
import { DatabaseError } from "../../../domain/errors/databaseError";
import { InvalidArgument } from "../../../domain/errors/invalidArgument";
import { League } from "../../../domain/league";

export class LeagueRepository {
  constructor({ mongodbConnector, logger }) {
    this.mongodbConnector = mongodbConnector;
    this.logger = logger;
  }

  async insert(leagueModel) {
    if (!(leagueModel instanceof League)) {
      throw new InvalidArgument("Wrong object send to repository");
    }
    const leaguePlainObject = leagueModel.get();
    try {
      const result = await this.mongodbConnector.db.collection("leagues").insertOne({
        ...leaguePlainObject,
        teams: leaguePlainObject.teams?.map((id) => ObjectId(id)) || [],
      });

      return new League({
        ...leagueModel.get(),
        _id: result.insertedId.toString(),
      });
    } catch (err) {
      this.logger.error(`TeamRepository.insert error : ${err.message}`);
      throw new DatabaseError(`Database query error`);
    }
  }

  async findOne(leagueId) {
    if (!ObjectId.isValid(leagueId)) {
      throw new InvalidArgument("Invalid leagueId param");
    }
    try {
      const result = await this.mongodbConnector.db.collection("leagues").findOne({
        _id: ObjectId(leagueId),
      });

      return result ? new League({ ...result, _id: result._id.toString() }) : null;
    } catch (err) {
      this.logger.error(`LeagueRepository.findAll error : ${err.message}`);
      throw new DatabaseError("Database query error");
    }
  }

  async findAll(query) {
    if (query && typeof query !== "string") {
      throw new InvalidArgument("Invalid query param");
    }
    try {
      const result = await this.mongodbConnector.db
        .collection("leagues")
        .find({
          ...(query && { name: { $regex: new RegExp(`^${query}`, "i") } }),
        })
        .sort({ name: 1 })
        .toArray();

      return result?.map((row) => ({
        ...row,
        _id: row._id.toString(),
        teams: row.teams?.map((id) => id.toString()) || [],
      }));
    } catch (err) {
      this.logger.error(`LeagueRepository.findAll error : ${err.message}`);
      throw new DatabaseError("Database query error");
    }
  }
}
