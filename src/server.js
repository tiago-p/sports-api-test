import "express-async-errors";
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import swaggerUi from "swagger-ui-express";
import swaggerDocument from "../config/openapi.json";
import container from "./container";
import helmet from "helmet";

const start = async () => {
  const app = express();
  const routes = container.resolve("router");
  const logger = container.resolve("logger");
  const config = container.resolve("config");
  const errorHandler = container.resolve("errorHandler");
  const mongodbConnector = container.resolve("mongodbConnector");

  mongodbConnector.connect(config.MONGODB_URL).then(() => {
    logger.info("MongoDb connection OK");
  });

  app.use(helmet());
  app.use(cors());
  app.use(bodyParser.json());
  app.use(routes());
  app.use("/", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  app.use(errorHandler.errorHandlerMiddleware);

  const port = process.env.PORT || config.port;

  app.listen(port, () => {
    logger.info(`Running on port ${port} in ${config.env} environment`);
  });
};

start();
