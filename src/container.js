import config from "./infra/config";
import { logger } from "./infra/logger";
import { createContainer, asClass, Lifetime, asValue, asFunction } from "awilix";
import { ApiController } from "./api/controllers/apiController";
import { routes } from "./api/router";
import { GetLeagues } from "./usecases/getLeagues";
import { GetLeagueTeams } from "./usecases/getLeagueTeams";
import { GetTeam } from "./usecases/getTeam";
import { LeaguesController } from "./api/controllers/leaguesController";
import { TeamsController } from "./api/controllers/teamsController";
import { errorHandler } from "./api/middlewares/errorHandlerMiddleware";
import { PlayerRepository } from "./infra/mongodb/repositories/playerRepository";
import { TeamRepository } from "./infra/mongodb/repositories/teamRepository";
import { LeagueRepository } from "./infra/mongodb/repositories/leagueRepository";
import { MongodbConnector } from "./infra/mongodb/mongodbConnector";
import { CreatePlayer } from "./usecases/createPlayer";
import { PlayersController } from "./api/controllers/playersController";
import { GetPlayers } from "./usecases/getPlayers";

const container = createContainer();

container.register({
  config: asValue(config),
  logger: asFunction(logger, { lifetime: Lifetime.SINGLETON }),
  router: asFunction(routes, { lifetime: Lifetime.SINGLETON }),
  errorHandler: asClass(errorHandler, { lifetime: Lifetime.SINGLETON }),
});

container.register({
  apiController: asClass(ApiController),
  leaguesController: asClass(LeaguesController),
  teamsController: asClass(TeamsController),
  playersController: asClass(PlayersController),
});

container.register({
  getLeagueTeams: asClass(GetLeagueTeams),
  getTeam: asClass(GetTeam),
  getLeagues: asClass(GetLeagues),
  createPlayer: asClass(CreatePlayer),
  getPlayers: asClass(GetPlayers),
});

container.register({
  mongodbConnector: asClass(MongodbConnector).singleton(),
  leagueRepository: asClass(LeagueRepository),
  teamRepository: asClass(TeamRepository),
  playerRepository: asClass(PlayerRepository),
});

export default container;
