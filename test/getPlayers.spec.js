import connect from "./utils/db";
import logger from "./utils/logger";
import { GetPlayers } from "../src/usecases/getPlayers";
import { Player } from "../src/domain/player";
import { PlayerRepository } from "../src/infra/mongodb/repositories/playerRepository";

const mockPlayer1 = {
  name: "Pierre-Emerick Aubameyang",
  position: "Forward",
  thumbnail: "https://www.thesportsdb.com/images/media/player/thumb/fnk3u51520755737.jpg",
  signin: {
    amount: 63750000,
    currency: "eur",
  },
  born: "1989-06-19",
};

const mockPlayer2 = {
  name: "Tiago P",
  position: "Forward",
  thumbnail: "https://www.thesportsdb.com/images/media/player/thumb/fnk3u51520755737.jpg",
  signin: {
    amount: 63750000,
    currency: "eur",
  },
  born: "1986-06-19",
};

describe("Players list", () => {
  let db;
  let getPlayers;
  let playerRepository;

  beforeAll(async () => {
    db = await connect();
    playerRepository = new PlayerRepository({ mongodbConnector: db.connection, logger });
    getPlayers = new GetPlayers({ playerRepository });
  });
  afterEach(async () => {
    await db.clear();
    jest.clearAllMocks();
  });
  afterAll(async () => {
    await db.disconnect();
  });

  test("should get empty array if no players", async () => {
    expect(await getPlayers.execute()).toStrictEqual([]);
  });

  test("should get players by name alphabetical order", async () => {
    const player1Model = await playerRepository.insert(new Player(mockPlayer1));
    const player2Model = await playerRepository.insert(new Player(mockPlayer2));

    expect(await getPlayers.execute()).toStrictEqual([
      new Player(player1Model).get(),
      new Player(player2Model).get(),
    ]);
  });
});
