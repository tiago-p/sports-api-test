import connect from "./utils/db";
import logger from "./utils/logger";
import { TeamRepository } from "../src/infra/mongodb/repositories/teamRepository";
import { LeagueRepository } from "../src/infra/mongodb/repositories/leagueRepository";
import { NotFoundError, ValidationError } from "../src/domain/errors";
import { Team } from "../src/domain/team";
import { GetLeagueTeams } from "../src/usecases/getLeagueTeams";
import { League } from "../src/domain/league";

const mockTeam = {
  name: "Benfica",
  thumbnail: "https://www.thesportsdb.com/images/media/team/badge/yvwvtu1448813215.png",
  players: [],
};

const mockLeague = {
  name: "French Ligue 1",
  sport: "soccer",
  teams: [],
};

describe("League teams", () => {
  let db;
  let teamRepository;
  let getLeagueTeamsUsecase;
  let leagueRepository;

  beforeAll(async () => {
    db = await connect();
    leagueRepository = new LeagueRepository({ mongodbConnector: db.connection, logger });
    teamRepository = new TeamRepository({ mongodbConnector: db.connection, logger });
    getLeagueTeamsUsecase = new GetLeagueTeams({ teamRepository, leagueRepository });
  });
  afterEach(async () => {
    await db.clear();
    jest.clearAllMocks();
  });
  afterAll(async () => {
    await db.disconnect();
  });

  test.each([
    ["numeric id", 111],
    ["empty id", ""],
    ["invalid id", "xx"],
    ["null id", null],
    ["undefined id", undefined],
  ])("should not get league teams with invalid league id : %s", async (_, value) => {
    await expect(getLeagueTeamsUsecase.execute(value)).rejects.toThrow(ValidationError);
  });

  test("should throw error if league not found", async () => {
    await expect(getLeagueTeamsUsecase.execute("5d2d8f60da07b95bb8f17170")).rejects.toThrow(
      NotFoundError,
    );
  });

  test("should get team leagues", async () => {
    const teamInserted = await teamRepository.insert(Team.create(mockTeam));
    const leagueInserted = await leagueRepository.insert(
      League.create({ ...mockLeague, teams: [teamInserted._id] }),
    );

    expect(await getLeagueTeamsUsecase.execute(leagueInserted._id)).toStrictEqual([
      new Team(teamInserted).get(),
    ]);
  });
});
