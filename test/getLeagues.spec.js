import connect from "./utils/db";
import logger from "./utils/logger";
import { LeagueRepository } from "../src/infra/mongodb/repositories/leagueRepository";
import { GetLeagues } from "../src/usecases/getLeagues";
import { League } from "../src/domain/league";
import { ValidationError } from "../src/domain/errors";

const mockLeague1 = {
  name: "French Ligue 1",
  sport: "soccer",
  teams: [],
};

const mockLeague2 = {
  _id: "5d2cdcf7da07b95bb8f16ed1",
  name: "English Premier League",
  sport: "soccer",
  teams: ["5d2d01fdda07b95bb8f16f0a", "5d2d02d7da07b95bb8f16f2a", "5d2d8f60da07b95bb8f17170"],
};

describe("League list", () => {
  let db;
  let getLeagues;
  let leagueRepository;

  beforeAll(async () => {
    db = await connect();
    leagueRepository = new LeagueRepository({ mongodbConnector: db.connection, logger });
    getLeagues = new GetLeagues({ leagueRepository });
  });
  afterEach(async () => {
    await db.clear();
    jest.clearAllMocks();
  });
  afterAll(async () => {
    await db.disconnect();
  });

  test.each([
    ["numeric query", 111],
    ["empty query", ""],
    ["single char query", "x"],
  ])("should not get leagues with invalid query : %s", async (_, value) => {
    await expect(getLeagues.execute(value)).rejects.toThrow(ValidationError);
  });

  test("should get empty array if no leagues", async () => {
    expect(await getLeagues.execute()).toStrictEqual([]);
  });

  test("should get leagues by name alphabetical order", async () => {
    const league1Model = await leagueRepository.insert(new League(mockLeague1));
    const league2Model = await leagueRepository.insert(new League(mockLeague2));

    expect(await getLeagues.execute()).toStrictEqual([
      new League(league2Model).get(),
      new League(league1Model).get(),
    ]);
  });

  test("should get filtered leagues by name", async () => {
    const league1Model = await leagueRepository.insert(new League(mockLeague1));
    await leagueRepository.insert(new League(mockLeague2));
    expect(await getLeagues.execute("fr")).toStrictEqual([new League(league1Model).get()]);
  });
});
