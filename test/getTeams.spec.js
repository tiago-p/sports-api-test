import connect from "./utils/db";
import logger from "./utils/logger";
import { TeamRepository } from "../src/infra/mongodb/repositories/teamRepository";
import { GetTeam } from "../src/usecases/getTeam";
import { NotFoundError, ValidationError } from "../src/domain/errors";
import { Team } from "../src/domain/team";

const mockTeam = {
  name: "Benfica",
  thumbnail: "https://www.thesportsdb.com/images/media/team/badge/yvwvtu1448813215.png",
  players: [],
};

describe("Team informations", () => {
  let db;
  let teamRepository;
  let getTeamUsecase;

  beforeAll(async () => {
    db = await connect();
    teamRepository = new TeamRepository({ mongodbConnector: db.connection, logger });
    getTeamUsecase = new GetTeam({ teamRepository });
  });
  afterEach(async () => {
    await db.clear();
    jest.clearAllMocks();
  });
  afterAll(async () => {
    await db.disconnect();
  });

  test.each([
    ["numeric id", 111],
    ["empty id", ""],
    ["invalid id", "xx"],
    ["null id", null],
    ["undefined id", undefined],
  ])("should not get team with invalid league id : %s", async (_, value) => {
    await expect(getTeamUsecase.execute(value)).rejects.toThrow(ValidationError);
  });

  test("should throw error if team not found", async () => {
    await expect(getTeamUsecase.execute("5d2d8f60da07b95bb8f17170")).rejects.toThrow(NotFoundError);
  });

  test("should get one team", async () => {
    const teamInserted = await teamRepository.insert(Team.create(mockTeam));

    expect(await getTeamUsecase.execute(teamInserted._id)).toStrictEqual(
      new Team({
        _id: teamInserted._id,
        ...mockTeam,
      }),
    );
  });
});
