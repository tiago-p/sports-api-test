const { MongoMemoryServer } = require("mongodb-memory-server");
const { MongodbConnector } = require("../../src/infra/mongodb/mongodbConnector");

export default async function connect() {
  const mongoServer = await MongoMemoryServer.create();
  const uri = mongoServer.getUri();

  const mongodbConnector = new MongodbConnector();
  await mongodbConnector.connect(uri);

  const clear = async () => {
    const collections = await mongodbConnector.db.listCollections().toArray();
    await Promise.all(
      collections.map((collection) =>
        mongodbConnector.db.collection(collection.name).deleteMany({}),
      ),
    );
  };

  const disconnect = async () => {
    mongodbConnector.disconnect();
    await mongoServer.stop();
  };

  const insert = async (collectionName, data) => {
    return await mongodbConnector.db.collection(collectionName).insertOne(data);
  };

  return {
    clear,
    disconnect,
    insert,
    connection: mongodbConnector,
  };
}
