import connect from "./utils/db";
import logger from "./utils/logger";
import { Player } from "../src/domain/player";
import { PlayerRepository } from "../src/infra/mongodb/repositories/playerRepository";
import { CreatePlayer } from "../src/usecases/createPlayer";
import { ValidationError } from "../src/domain/errors";

const mockPlayer = {
  name: "Tiago P",
  position: "Forward",
  thumbnail: "https://www.thesportsdb.com/images/media/player/thumb/fnk3u51520755737.jpg",
  signin: {
    amount: 63750000,
    currency: "eur",
  },
  born: "1986-06-19",
};

describe("Create player", () => {
  let db;
  let createPlayer;
  let playerRepository;

  beforeAll(async () => {
    db = await connect();
    playerRepository = new PlayerRepository({ mongodbConnector: db.connection, logger });
    createPlayer = new CreatePlayer({ playerRepository });
  });
  afterEach(async () => {
    await db.clear();
    jest.clearAllMocks();
  });
  afterAll(async () => {
    await db.disconnect();
  });

  test.each([
    ["numeric name", { name: 111 }],
    ["empty name", { name: "" }],
    ["empty position", { position: "" }],
    ["empty born", { messages: null }],
    ["numeric thumbnail", { thumbnail: 123 }],
    ["with unexpected data", { hello: "test" }],
  ])("should not create player with invalid values : %s", async (_, values) => {
    await expect(createPlayer.execute({ ...mockPlayer, ...values })).rejects.toThrow(
      ValidationError,
    );
  });

  test("should create player with valid data", async () => {
    expect(await createPlayer.execute(mockPlayer)).toStrictEqual(
      new Player({
        ...mockPlayer,
        born: new Date("1986-06-19"),
        _id: expect.any(String),
      }),
    );
  });
});
